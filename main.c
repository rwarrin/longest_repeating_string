/**
 * Finds the longest repeated string within some text.
 *
 * Uses a Suffix Array structure to create N number of strings where N is the
 * length of the original text.
 *
 * The Suffix Array is then sorted using a simple hand made Quick Sort. By
 * sorting all of the strings alphabetically will cause repeating strings to
 * appear next to eachother so we can simply find the longest sequence in string
 * I and I+1.
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static inline int
LongestCommonSequence(char *Left, char *Right)
{
	int Result = 0;
	for(; *Left++ == *Right++; ++Result);
	return(Result);
}

static inline void
SwapStringPointers(char **A, char **B)
{
	char *Temp = *B;
	*B = *A;
	*A = Temp;
}

static inline void
QuickSort(char **Array, int Lower, int Upper)
{
	if(Lower >= Upper)
	{
		return;
	}

	char *PartitionValue = Array[Lower];
	int Partition = Lower;

	for(int TestIndex = Lower + 1; TestIndex <= Upper; ++TestIndex)
	{
		if(strcmp(Array[TestIndex], PartitionValue) < 0)
		{
			SwapStringPointers(&Array[++Partition], &Array[TestIndex]);
		}
	}
	SwapStringPointers(&Array[Lower], &Array[Partition]);

	QuickSort(Array, Lower, Partition - 1);
	QuickSort(Array, Partition + 1, Upper);
}

int
main(int argc, char **argv)
{
	if(argc != 2)
	{
		fprintf(stderr, "Usage: %s [filename]\n", argv[0]);
		return 1;
	}

	FILE *File = fopen(argv[1], "rb");
	if(!File)
	{
		fprintf(stderr, "Could not open file '%s'\n", argv[1]);
		return 2;
	}

	int FileSize = 0;
	fseek(File, 0, SEEK_END);
	FileSize = ftell(File);
	fseek(File, 0, SEEK_SET);

	char *FullText = (char *)malloc(sizeof(char) * FileSize + 1);
	if(!FullText)
	{
		fprintf(stderr, "Failed to allocate memory for file.\n");
		return 3;
	}

	char **SubstringPointers = (char **)malloc(sizeof(char *) * FileSize + 1);
	if(!SubstringPointers)
	{
		fprintf(stderr, "Failed to allocate memory for pointers\n");
		return 4;
	}

	fread(FullText, 1, FileSize, File);
	for(int Index = 0; Index < FileSize; ++Index)
	{
		SubstringPointers[Index] = &FullText[Index];
	}

	QuickSort(SubstringPointers, 0, FileSize - 1);

	int MaxCommonSequenceLength = -1;
	char *CommonSequence = 0;
	for(int Index = 0; Index < FileSize - 1; ++Index)
	{
		int SequenceLength = LongestCommonSequence(SubstringPointers[Index], SubstringPointers[Index + 1]);
		if(SequenceLength > MaxCommonSequenceLength)
		{
			MaxCommonSequenceLength = SequenceLength;
			CommonSequence = SubstringPointers[Index];
		}
	}

	if(MaxCommonSequenceLength > -1)
	{
		printf("Longest Common Sequence: %d characters\n%.*s\n", MaxCommonSequenceLength, MaxCommonSequenceLength, CommonSequence);
	}
	else
	{
		printf("No common sequences\n");
	}

	fclose(File);
	free(FullText);
	free(SubstringPointers);

	return 0;
}
